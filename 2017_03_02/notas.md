#### Kaleidos - Perspectivas profesionales del software libre

Kaleidos: de forma bella. Salió de una votación interna. 

- Desarrollo de software libre

- Desarrollo web

Son 21 personas. No es una empresa típica de tecnología. Intentan
integrar todos los perfiles dentro del desarrollo de un
proyecto. Intentan cambiar el hecho de que todo el mundo piensa que el
software libre es "feo y poco usable"

Montaron la empresa porque no les gustaban "las cosas que estaban
haciendo ahí fuera".

Uno de los socios (Pablo) había creado el departamento de software
libre de una empresa conocida.

Olé:

- Trabajar con la administración pública es el mal.

- Contratar gente para que luego estén por ahí perdidos, es el mal.

- Intentan que el equipo que va a heredar sus proyectos estén
  integrados desde el principio, porque no quieren realizar
  mantenimiento de sus productos.

Cuentan la historia de Stallman y su impresora. Tuvo que modificar el
driver de la impresora para que le avisara cuando sus archivos estaban
impresos. Se modificó el driver y ya no podía modificarlo, (etc,
búscalo). Le dio una pataleta molona y se creó el movimiento del
software libre.

Free VS free. -> Libertad de discurso en vez de gratuidad

Kaleidos tiene ingresos que los consiguen de diferentes formas. Tienen
partes que liberan y partes que no.

Cuentan el rollo del open source: las 4 libertades:

- Libertad para ejecutar un programa

- Libertad para analizar el código

- Libertad para redistribuir

- Libertad para modificar y compartir

Si no se cumplen estas 4 condiciones, no se puede hablar de código
abierto/libre.

En ningún momento se dice que el software tiene que ser gratis.

Tienen transparencia para ver qué se está haciendo con el dinero, cómo
se está gastando, etc.

Cuentan todo lo de las licencias de software. Búscalo si haces el
resumen de esta conferencia. Además, mencionan las dobles licencias
(licencia de comunidad y licencia de empresa). Cuidado! Si no pones
licencia al subir algo a github, por defecto el código es propietario.

ME ENCANTAN ESTOS TÍOS.

"Gente responsable que sabe lo que tiene que hacer en cada
momento". En la empresa hay CEO, CIO y CTO: en esta empresa sólo sirve
para comerse más marrones. Las estructuras piramidales nunca les había
gustado.

Se va el dinero en cosas que molan, no en mantener estructuras "de
caca". EAA. Pagan los viajes para ir a mantener los proyectos que
tienen en Londres, ir a congresos, etc. 90% en sueldos, 10% restante
en mantener las oficinas y pagar ese tipo de cosas.

Documento WTF: jornadas de teletrabajo, elegir tu propia jornada,
dedicar tiempo a cosas que realmente importan.

La empresa se creó en 2011. Al principio fue muy difícil conseguir
dinero.

"Equipos de ninjas silenciosos, efectivos y altamente entrenados." No
hay nadie encima dándoles con un látigo. El propio cliente suele estar
dentro del equipo.

Desarrollo ágil: a diferencia de sitios como los bancos, usan
metodologías ágiles.

Hasta que el cliente no usa la herramienta, no sabe lo que quiere. La
base para que las cosas funcionen es tener mucha comunicación con el
cliente, iteraciones muy cortas, feedback continuo. Se definen una
serie de tareas como una serie de "wish lists". Se atacan las
prioritarias y se pide feedback al cliente. Si no le gusta algo que es
importante, se modifica. Si es menos importante, se continúa con la
lista de deseos. Usan Scrum y Kanban (taiga). 

La empresa tiene una "cultura geek". 

Proyectos: procesamiento de vídeo, reconocimiento facial.

### ¿Cómo se mantienen fieles a sus principios?

- Manifiesto (el que está en su web): In the beginning, it's the
  people, code should be beautiful,...)

- PIWEEK: dos semanas al año, en junio y en diciembre, se saca un
  tiempo y cada persona hace lo que le da la gana con dos únicas
  condiciones: tiene que ser software libre y los viernes tienes que
  contar lo que estás haciendo.

- Comité ético: aprueban proyectos y comprueban que favorece la
  diversidad, mejora el mundo, etc. Ven que tenga sentido y a veces
  dicen que no.

- Cualquier persona individualmente puede renunciar a contribuir a un
  proyecto por motivos éticos.

- Impacto social en el mundo

- Conciliación laboral - familiar. 


alonso.torres@kaleidos.net - alotor
alejandro.alonso@kaleidos.net - _superalex_
