### Notas conferencia Amazon 06-03-17

Pizza teams:
- 6-12 personas
- Tienen que tener todos los roles
- Se encargan de dirigirse a ellos mismos
- Se llaman así porque tiene que ser un equipo en el cual se necesiten como máximo dos pizzas.

## No se trata del tamaño del equipo

Ciclo de desarrollo:

- Captura requisitos
  - Requisitos técnicos
  - Requisitos de negocio
- Llevarlo a un desarrollo

## Definición de producto

Cada equipo decide:
- Requisitos de negocio
- Technical needs
- Iniciativas de seguridad
- Métricas
- Tecnología necesaria

## Metodología
Cada equipo define:
- Metodologías ágiles: Kanban, Scrum, XP, Waterfall
- Lenguaje de programación
- Arquitectura que quieren seguir


## Agile Manifesto
- Individuals and interactions over processes and tools
- Working software over comprehensive documentation
- Customer colaboration over contract negotiations
- Responding to change over following a plan


## Lenguajes más usados
- Java
- HTML
- JavaScript
- CSS
- c++

## Ownership
- El equipo es dueño de la calidad
- Los desarrolladores son dueños del equipo
- El equipo elige la estrategia de calidad
- Centrarse en detectar fallos rápidos en vez de intentar evitarlos
- Compartir responsabilidad de calidad en la compañía

## Producto
- El equpo se encaga de las operaciones de equipo
- Opera sobre la infraestructura
- Decide la estrategia de despliegue
- No hay equipo de soporte dedicado
  - Cada uno se come su propia mierda
- El equipo es dueño de las métricas, las monitorean.

## Trustu the Individuals

- Invertir en contratar a personas
- Invertir en formar a tus ingenieros
- Aceptar los fallos como parte del proceso de innovación
