### Patentabilidad, propiedad intelectual

#### 13 de marzo, 2017

##### Charla impartida por Eduardo Martín, de la oficina española de patentes y marcas

http://wikis.fdi.ucm.es/ELP/images/5/54/PATENTABILIDAD_INVENCIONES_IMPLEMENTADAS_EN_ORDENADOR_30_NOV_2015_UCM_INFORMATICA_.pdf

Patente: 
Requiere que se publique:
- Descripción de la invención y su realización
- Definición de la materia protegida

Los documentos de patentes tienen una estructura uniforme, similar en todo el mundo. 
- Primera página: titular e inventor
- Memoria descriptiva (técnica)
- Reivindicaciones
-...? (pasa muy rápido!, no lo pillé :/)
- Informe sobre el estado de la técnica (IET): búsqueda de todo aquello que se ha presentado o publicado en cualquier medio y cualquier idioma y que puede afectar al contenido de lo que se quiere publicar. 

Patentes regidas por leyes nacionales, europeas e internacionales. 

Se puede partir de algo que ya está patentado, realizar pequeñas modificaciones y beneficiarse de una nueva patente. No siempre es necesario que sea 100% nuevo. 

Vaya conferencia señores... Hasta aquí puedo leer, porque madre mía!