# Conferencia sobre The Future of IT Workforce
### _Autor: Miguel Angel Rojas_
### _20 de Febrero del 2017_

## Objetivo del departamento de IT

Hay que crear valor de negocio:

- Mantenerse siempre actualizados.
- No centrarse en instalación/despliegue/mantenimiento.
- Mejorar tecnología y procesos.

## Estrucura departamento IT

_Ver PDF diapositiva nº7_

- Estructura piramidal.
    - El flujo de negocio va de izquierda a derecha.
    - Las decisiones de negocio van de arriba a abajo.
- Cada actor tiene su funcion.

## Cambio de tecnología

- Primer cambio
  - Redes sociales
  - Nube
  - Marketing digital
- Siguiente generación
  - _Digital Bussinness_
  - Sistemas autónomos

## Conclusiones

- Hay que ir cambiando
- Nuestra carrera no acaba con la universidad
- Tener el perfil más valorado
- Trabajar en lo que nos gusta

## Comentarios del oyente

- Este tio viene a vendernos su compañía.
- Ahora resulta que Gas Natural es la puta hostia mejor que facebook o  airbnb.
- Usa imágenes de stock que hemos usado todos.
- Nos está contanto básicamente todo lo que dimos en DGPSI.
