### Seguridad en IoT

- De la empresa Tecsup, viene de la universidad de Lima.

- Más info de Tecsup en el pdf, que tiene colgado en http://bit.ly/ucm2017

#### Control de un proceso

Temperatura, nivel de agua...

Aparece un nuevo elemento en electrónico: PLC -> controlador lógico programable.


Nuevas cosas:

- Arduinos -> es un sistema operativo embebido, no es como tal una computadora. 

- Raspberri pi -> es una computadora, con su propio sistema operativo. 

- Galileo

Son computadoras muy pequeñas que se pueden programar y colocar en
cualquier lado, incorporándoles sensores, etc.

- Automatizar una silla. ¿Qué podemos meterle? Adaptarse si eres un
  hombre, mujer, niño, altura, etc., para ver cómo te sientas, para
  controlar asistencia en las clases, para llevar estadísticas de
  asistencia, peso, glucosa, colesterol,...

- A partir del control de peso, si también ponemos un controlador en
  el frigorífico, entonces se podrían comunicar entre pares para
  compartir información. Podrían decidir ponerte a dieta si has
  engordado, y asegurarse de que en el frigorífico tienes lo necesario
  para la dieta.


#### Datos en la nube

- Toda la información que se recoge se suele subir a la nube desde los
  dispositivos mencionados. Con todos los datos que se recogen, se
  pueden hacer analytics. A partir de ello, se pueden hacer cosas como:

  - Predicción, mantenimiento de aulas, análisis predictivos,
    cuestiones económicas para tomar decisiones...



Internet de las cosas: Red de objetos con electrónica, software y que
permiten intercambiar datos y generar conocimiento para la toma de
decisiones.

Para recabar información, se hacen encuestas, etc. Con la información
que se recoge, los de marketing pueden trabajar para realizar campañas
que puedan beneficiar a la compañía.

|   |
|---|
| Sabiduría |
| Conocimiento |
| Información |
| Datos |


#### Telejampiq

"El médico en casa". -> Telemedicina para cuidados paliativos. 

#### IoT en el futuro

- Millones de dispositivos conectados a internet

  - Relojes

  - Lavadoras

  - Frigoríficos

  - ...


Todos los dispositivos interconectados entre ellos. Al estar
conectados a internet, todos ellos son susceptibles a ataques.

El TIPO menciona Stuxnet como ataques conocidos. 

_Me perdí. Espero que no fuera importante_

- Cambiar las contraseñas por defecto para que no hagan ataques.

Otros ataques:

- A Jeep Cherokee (Carjackers): toman el control de tu coche. 

- DEFCON


En algún momento mencionó Mirai. ¿Qué narices era eso? Si te toca
hacer la presentación sobre esto, míralo.
