### Aplicaciones prácticas de machine learning (McKinsey & Company)

##### Ponentes:

- Almudena Sanz

- Álvaro Sánchez

- Marta López

##### Agenda:

- An intro to McKinsey

- My McKinsey

- Careers with us

#### An intro to McKinsey.

Usar modelos de machine learning para hacer predicciones que se puedan aplicar
a casos reales en la industria para todos sus clientes.

### Alvaro - Primer Ponente.

- Video: publicidad de la empresa.
  - Habla del análisis de McKinsey.
  - Salen imágenes con preguntas.
- Solucionar problemas en los que una predicción te puede dar ventajas con respecto a la competencia.
- Mazo publicidad sombre la empresa.

### Almudena - Segunda ponente: Data Scientist LOKO

- A esta tía le han puesto aquí por voluntariado obligatorio.
- McKinsey -> DATPS REALES. EMPRESAS REALES. IMPACTO REAL.
- Otra que nos vende la empresa.

### Carceles en EEUU

- Saber qué reclusos se iban a meter de hostias.
- Saber qué factores incitaban a la agresividad o delitos.
- Modelo de prediccion de datos
  - Elegir información de las babses de datos (Edad, tipo de delitos, pretenencia a una banda).
    - R o Ptyhon
    - Modelo de entrenamiento:
      - Random forest
      - Gradient boisting.
    - Métricas para analizar la calidad de predicción (YINI).

### Optmización de CAPEX 3g/4g/LTE

- ¿Cómo invertir la pasta para crear antenas en qué sitios?
- Estudio de cómo optimizar:
  - Se parece a otros problemas de otras áreas.
    - Datos de telecomunicación.
    - Ruta por cliente de a qué antena se iba conectando.
    - Cómo se parecían los clientes entre sí:
      - K-MEANS
      - Clientes que toman rutas parecidas.
      - Clusters que relacionaban a la gente que tomaba rutas similares.
  - Invertir en las zonas de cada uno de los clusters.
  - Mapa de calor:
    - Dónde se gastaban más datos.
    - Cuánta gente se conecta.
  - Satisfacción del cliente.

# FIN DE LA PARTE DIVERTIDA. COMIENZA OTRA VEZ PUBLICIDAD.

- Esta tía se explica mal si se trata de empresa.
- ENTRA EL RECRUITER ON STAGE!!!!
- YA ES DESCARADO QUE NOS QUIEREN RECLUTAR.
