# Conferencia sobre Chatbots e IoT
### _Autor: Mario López López, de Accenture_
### _22 de Febrero del 2017_

Empieza mencionando la falta de diversidad de género en las empresas de ingeniería.

### Bots, asistentes digitales

- Un humano interactúa con una máquina

- La máquina está preparada, no sólo para hacer preguntas y respuestas, sino que poco a poco la base de datos va creciendo y aprende poco a poco.

- La tecnología tiene que estar asociada a un problema que hay en la actualidad.

- Propósito de los asistentes virtuales: proporcionar un servicio parecido al que te pudiera dar un humano, pero sin que sea necesario que esté el humano. Ventajas: rapidez de interacción, extensión de los horarios, ahorro en costes...

### ¿Qué es un asistente digital?

- No humano dando servicio a un humano.

- La interfaz con el usuario es típicamente una aplicación de mensajería. 

_Ecosistema del chatbot_

|    |           
|----------|
| Interfaz de usuario |
| Backend |
| Lenguaje natural |
| Machine learning |
| Otras soluciones |

Lo interesante son los intérpretes de lenguaje natural. Es capaz de
seleccionar ciertas palabras de la interacción y te da una
respuesta. Se almacenan las peticiones para que no haya que realizar
siempre los mismos cálculos. Se pueden realizar interacciones más allá
de las preguntas y las respuestas gracias al aprendizaje automático y
a llevar a cabo ciertas correlaciones.

Las respuestas deberían ser personalizadas. Por ejemplo con iberia, si
estás intentando contratar un vuelo y ya eres usuario, debería ser
capaz el sistema de darte los precios de los vuelos que les estás
preguntando en función de los puntos que tengas.

Las respuestas deben estar contextualizadas a lo que has hecho
anteriormente en la aplicación, a tu localización, a tu personalidad,
a tu estado de ánimo,... Cada vez que abras la aplicación, la forma en
la que veas la información debería ser distinta y adaptada a las
interacciones anteriores.

Nos presenta un chatbot que se llama Charlie. Es un asistente de
viajes. Usan apis de terceros que están abiertas. Así, pueden
centrarse en la parte de machine learning.

#### #frasecélebre: TIENES A CHARLIE IGUAL QUE HABLAS CON TU PRIMA

Con google analytics integran una serie de métricas para conocer el
uso de la aplicación.

Además, en el chatbot todo se queda grabado y se analiza (equivalente
a la grabada de llamadas de los call centre). El modelo se puede
retroalimentar y las reglas se amplían.

### Internet of things

Me perdí. Sorry about that.

### Connected health

- Se tiene mucha información sobre las personas.

- Hay dispositivos muy complejos que son capaces de monitorizar toda
  la información. Puede interesar o no interesar desde el punto de
  vista econónimo.

- Seguros: pulseras de salud que te den recomendaciones de salud y que
  la póliza de seguros esté relacionada con cómo de saludable seas.

- Médicos: puede ocurrir que haya médicos que solo quiera tener
  pacientes sanos y no quiera atender a los demás.

- Proyectos chulos que tienen:

  - Proyecto para las personas mayores, para que estén conectados con
    sus asistentes, que les recuerden que den paseos, que se tomen las
    pastillas...

  - Para la precisión en agricultura: ¿Cómo de sanos son mis animales? ¿Cuándo están comiendo mis vacas? ¿Cómo de buena va a ser mi cosecha?
